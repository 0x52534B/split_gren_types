import { splitTypes } from "./index";

describe("splitTypes", () => {
  test("basic", () => {
    const strType1 =
      "{ init : model, view : model -> Html.Html msg, update : msg -> model -> model } -> Platform.Program {} model msg";
    const strType1Result = [
      "{ init : model",
      ", view : model -> Html.Html msg",
      ", update : msg -> model -> model",
      "} -> Platform.Program {} model msg",
    ];

    expect(splitTypes(strType1)).toStrictEqual(strType1Result);
  });

  test("type2", () => {
    const strType2 =
      "{ init : flags -> Url -> Key -> { model : model, command : Cmd msg }, view : model -> Document msg, update : msg -> model -> { model : model, command : Cmd msg }, subscriptions : model -> Sub msg, onUrlRequest : UrlRequest -> msg, onUrlChange : Url -> msg } -> Program flags model msg";
    const strType2Result = [
      "{ init : flags -> Url -> Key -> { model : model, command : Cmd msg }",
      ", view : model -> Document msg",
      ", update : msg -> model -> { model : model, command : Cmd msg }",
      ", subscriptions : model -> Sub msg",
      ", onUrlRequest : UrlRequest -> msg",
      ", onUrlChange : Url -> msg",
      "} -> Program flags model msg",
    ];

    expect(splitTypes(strType2)).toStrictEqual(strType2Result);
  });

  test("record", () => {
    const strRecord = "{ title : String, body : Array (Html msg) }";
    const strRecordResult = [
      "{ title : String",
      ", body : Array (Html msg)",
      "}",
    ];

    expect(splitTypes(strRecord)).toStrictEqual(strRecordResult);
  });
  test("decoder lazy", () => {
    const strRecord = "({} -> Decoder a) -> Decoder a";
    const strRecordResult = ["({} -> Decoder a)", "-> Decoder a"];

    expect(splitTypes(strRecord)).toStrictEqual(strRecordResult);
  });
  test("task sleep", () => {
    const strRecord = "Float -> Task x {}";
    const strRecordResult = ["Float -> Task x {}"];

    expect(splitTypes(strRecord)).toStrictEqual(strRecordResult);
  });

  test("decoder Object Primitives", () => {
    const strRecord = "Array String -> Decoder a -> Decoder a ";
    const strRecordResult = ["Array String", "-> Decoder a", "-> Decoder a"];

    expect(splitTypes(strRecord)).toStrictEqual(strRecordResult);
  });

  test("decoder keyValuePairs", () => {
    const strRecord =
      "Decoder a -> Decoder (Array { key : String, value : a })";
    const strRecordResult = [
      "Decoder a",
      "-> Decoder (Array { key : String, value : a })",
    ];

    expect(splitTypes(strRecord)).toStrictEqual(strRecordResult);
  });

  test("comparable", () => {
    const strRecord =
      "(comparable -> a -> result -> result) -> (comparable -> a -> b -> result -> result) -> (comparable -> b -> result -> result) -> Dict comparable a -> Dict comparable b -> result -> result";
    const strRecordResult = [
      "(comparable -> a -> result -> result)",
      "-> (comparable -> a -> b -> result -> result)",
      "-> (comparable -> b -> result -> result)",
      "-> Dict comparable a",
      "-> Dict comparable b",
      "-> result",
    ];

    expect(splitTypes(strRecord)).toStrictEqual(strRecordResult);
  });

  test("decoder map8", () => {
    const strRecord =
      "(a -> b -> c -> d -> e -> f -> g -> h -> value) -> Decoder a -> Decoder b -> Decoder c -> Decoder d -> Decoder e -> Decoder f -> Decoder g -> Decoder h -> Decoder value";
    const strRecordResult = [
      "(a -> b -> c -> d -> e -> f -> g -> h -> value)",
      "-> Decoder a",
      "-> Decoder b",
      "-> Decoder c",
      "-> Decoder d",
      "-> Decoder e",
      "-> Decoder f",
      "-> Decoder g",
      "-> Decoder h",
      "-> Decoder value",
    ];

    expect(splitTypes(strRecord)).toStrictEqual(strRecordResult);
  });
});
