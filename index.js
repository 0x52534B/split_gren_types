export function splitTypes(strType) {
  if (
    strType.indexOf("Decoder") !== -1 ||
    strType.indexOf("comparable") !== -1
  ) {
    return splitDecoderSignature(strType);
  }

  const typeArr = strType.split(",");
  const indexToSkip = [];

  let types = typeArr.map((line, index) => {
    line = line.trim();
    if (index === 0) {
      return line;
    }
    return `, ${line}`;
  });

  types = types
    .map((line, index) => {
      if (indexToSkip.indexOf(index) >= 0) {
        return;
      }
      if (line.indexOf("-> {") >= 0) {
        indexToSkip.push(index + 1);
        return `${line}${types[index + 1]}`;
      }
      return line;
    })
    .filter((line) => Boolean(line));

  const len = types.length;

  if (len > 1) {
    const lastLine = types[len - 1].trim();
    let index = lastLine.indexOf("} ->");

    if (index >= 0) {
      types = handleLastLine(types, lastLine, index);
    } else {
      // end of a record
      index = lastLine.indexOf("}");

      if (index === lastLine.length - 1) {
        types = handleLastLine(types, lastLine, index);
      }
    }
  }

  return types.map((line) => line.trim());
}

function splitDecoderSignature(strType) {
  let str = strType;
  let types = [];
  while (str.length) {
    const indexOfOpen = str.indexOf("(");
    const indexOfClose = str.indexOf(")");

    if (indexOfOpen !== 0) {
      str = str.trim();
      if (str.indexOf("->") !== -1) {
        types = types.concat(
          str
            .split("->")
            .filter((line) => Boolean(line.trim()))
            .map((line) => `-> ${line.trim()}`),
        );
        if (types[0].startsWith("->")) {
          types[0] = types[0].substring(2).trim();
        }
      } else {
        types = types.concat(str);
      }

      str = "";
      break;
    }

    let prefix = "";
    if (types.length) {
      prefix = "-> ";
    }

    types = types.concat(
      `${prefix}${str.substring(indexOfOpen, indexOfClose + 1)}`,
    );
    str = str.substring(indexOfClose + 1);
  }
  return types;
}

function handleLastLine(types, lastLine, index) {
  const len = types.length;
  const firstPart = lastLine.substring(0, index);
  const secPart = lastLine.substring(index);
  types[len - 1] = firstPart;
  types[len] = secPart;
  return types;
}
